# Bots

## Statistics
* [@mastodonusercount@lou.lt](https://social.lou.lt/@mastodonusercount) - Count Mastodon users (in the Federivse)

## Productivity
* [@dicebot@cmx.im](https://cmx.im/@dicebot) - A bot rolling dice. See [documentation (Chinese)](http://docs.trpg.io/dicebot/).